import React, { Component } from "react";
import axios from 'axios';

export default class Login extends Component {
    state = {
        usuarios: [],
    }    
    componentDidMount(){
        axios.get(`http://localhost:8080/api/usuarios?limite=20`)
        .then(res => {
            const usuarios = res.data.usuarios;
            this.setState({ usuarios });
        })
    }

    renderList(){

        return this.state.usuarios.map((data)=>{
  
          return(
            <tr>
              <td>{data.nombre}</td>
              <td>{data.correo}</td>
              <td>{data.estado ? 'Activo' : 'Inactivo'}</td>
              <td>{data.rol == 'ADMIN_ROLE' ? 'Admin' : 'Usuario'}</td>
            </tr>
          )
  
        })
  
      }
    render() {
        return (
            <root>
                <h3>LISTA DE USUARIOS</h3>
            <table class="table table-bordered order-table ">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Correo</th>
                  <th>Estado</th>
                  <th>Rol</th>
                </tr>
              </thead>
              <tbody>
                 {this.renderList()}
              </tbody>
            </table>
            </root> 
        );
    }
}