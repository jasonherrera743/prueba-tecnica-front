import React, { Component } from "react";
import axios from 'axios';

export default class SignUp extends Component {
    state = {
        nombre: '',
        correo: '',
        password: ''
      }    

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;    
        
        this.setState({
          [name]: value,
        })
    } 

    handleSubmit = (e) => {
        e.preventDefault();    
        const user = {
            nombre: this.state.nombre,
            correo: this.state.correo,
            password: this.state.password,
            rol: 'USER_ROLE'
        };
      
        return axios({
            method: 'post',
            url: 'http://localhost:8080/api/usuarios',
            data: user
        })
        .then(res => { 
            if (res) {
                this.routeChange();
            }
        })
        .catch(error => console.log(error.response.data)); 
    }  

    routeChange() {
        let path = `/sign-in`;
        this.props.history.push(path);
      }

    render() {
        return (
            <root>
            <form onSubmit = {this.handleSubmit}>
                <h3>Registrate !</h3>

                <div className="form-group">
                    <label>Nombre</label>
                    <input name='nombre' onChange={this.handleChange} value={this.state.nombre} type="text" className="form-control" placeholder="Ingrese su nombre" />
                </div>

                <div className="form-group">
                    <label>Email</label>
                    <input name='correo' onChange={this.handleChange} value={this.state.correo} type="email" className="form-control" placeholder="Ingrese Email" />
                </div>

                <div className="form-group mb-4">
                    <label>Password</label>
                    <input name='password' onChange={this.handleChange} value={this.state.password} type="password" className="form-control" placeholder="Ingrese una contraseña" />
                </div>

                <button type="submit" className="btn btn-dark btn-lg btn-block">Registrarse</button>
            </form>
            </root>
        );
    }
}