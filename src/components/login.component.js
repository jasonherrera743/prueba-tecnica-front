import React, { Component } from "react";
import axios from 'axios';

export default class Login extends Component {
    state = {
        correo: '',
        password: ''
      }    

    handleChange = (e) => {
        const target = e.target;
        const name = target.name;
        const value = target.value;    
        
        this.setState({
          [name]: value,
        })
    } 

    handleSubmit = (e) => {
        e.preventDefault();    
        const user = {
            correo: this.state.correo,
            password: this.state.password,
        };
      
        return axios({
            method: 'post',
            url: 'http://localhost:8080/api/auth/login',
            data: user
        })
        .then(res => { 
            if (res) {
                this.routeChange();
            }
        })
        .catch(error => console.log(error.response.data)); 
    } 
    
    routeChange() {
        let path = `/home`;
        this.props.history.push(path);
    }

    render() {
        return (
            <form onSubmit = {this.handleSubmit}>

                <h3>Ingresa</h3>

                <div className="form-group">
                    <label>Email</label>
                    <input name='correo' onChange={this.handleChange} type="email" className="form-control" placeholder="Ingrese email" />
                </div>

                <div className="form-group mb-4">
                    <label>Password</label>
                    <input name='password' onChange={this.handleChange} type="password" className="form-control" placeholder="Ingrese password" />
                </div>

                <button type="submit" className="btn btn-dark btn-lg btn-block">Ingresar</button>
            </form>
        );
    }
}